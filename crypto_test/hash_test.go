package crypto_test

import (
	"crypto/sha256"
	"sync"
	"testing"
)

func BenchmarkSHA256_Single(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha256.Sum256(MSG[i%MSGCNT])
	}
}

func BenchmarkSHA256_GoRoutine(b *testing.B) {
	wg := sync.WaitGroup{}

	for i := 0; i < GOCNT; i++ {
		wg.Add(1)
		go func() {
			for j := 0; j < b.N/GOCNT; j++ {
				sha256.Sum256(MSG[j%MSGCNT])
			}
			wg.Done()
		}()
	}
	wg.Wait()
}
