package crypto

// #cgo CFLAGS: -g -Wall -Wno-format -Wno-unused-result -O2 -I/Users/kysee/go/src/github.com/bitcoin-core/secp256k1/include
// #cgo LDFLAGS: -L./secp256k1/ -lsecp256k1.0
// #include "secp256k1/batch.c"
import "C"
import (
	"unsafe"
)

var CTX *C.secp256k1_context

func init() {
	// //#cgo CFLAGS: -g -O2 -Wall -Wextra -Wno-unused-function -DUSE_BASIC_CONFIG=1 -I/Users/kysee/projects/ECDSA-OpenCL/bitcoin-core/secp256k1/include -v
	CTX = C.secp256k1_context_create(C.SECP256K1_CONTEXT_SIGN | C.SECP256K1_CONTEXT_VERIFY)
}

func VerifySig(sig, msg32, pubkey []byte) int {
	ret := C.secp256k1_ecdsa_verify_single(CTX,
		(*C.uchar)(unsafe.Pointer(&sig[0])),
		(*C.uchar)(unsafe.Pointer(&msg32[0])),
		(*C.uchar)(unsafe.Pointer(&pubkey[0])),
		(C.ulong)(len(pubkey)))

	return int(ret)
}

func VerifySigBatch(sigs, msg32s, pubkeys [][]byte) int {
	sigcnt := len(sigs)

	_sigs := make([]C.secp256k1_ecdsa_signature, sigcnt)
	_msg32s := make([]C.secp256k1_msg32, sigcnt)
	_pubkeys := make([]C.secp256k1_pubkey, sigcnt)

	for i := 0; i < sigcnt; i++ {
		C.secp256k1_ecdsa_signature_parse_compact(CTX, (*C.secp256k1_ecdsa_signature)(unsafe.Pointer(&_sigs[i])), (*C.uchar)(unsafe.Pointer(&sigs[i][0])))
		C.memcpy(unsafe.Pointer(&_msg32s[i].data[0]), unsafe.Pointer(&msg32s[i][0]), 32)
		C.secp256k1_ec_pubkey_parse(CTX, (*C.secp256k1_pubkey)(unsafe.Pointer(&_pubkeys[i])), (*C.uchar)(unsafe.Pointer(&pubkeys[i][0])), (C.ulong)(len(pubkeys[i])))
	}

	ret := C.secp256k1_ecdsa_verify_batch(CTX,
		(*C.secp256k1_ecdsa_signature)(unsafe.Pointer(&_sigs[0])),
		(C.ulong)(sigcnt),
		(*C.secp256k1_msg32)(unsafe.Pointer(&_msg32s[0])),
		(*C.secp256k1_pubkey)(unsafe.Pointer(&_pubkeys[0])))
	return int(ret)
}
