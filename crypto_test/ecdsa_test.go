package crypto_test

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"github.com/btcsuite/btcd/btcec"
	ethcrypto "github.com/ethereum/go-ethereum/crypto"
	ethsecp256k1 "github.com/ethereum/go-ethereum/crypto/secp256k1"
	"github.com/stretchr/testify/require"
	"golang.org/x/crypto/ripemd160"
	crypto_test "idear-research/crypto_test"
	"math/big"
	"runtime"
	"testing"
	"time"
)

var (
	GOCNT  = 10
	MSGCNT = 10000
	MSG    = make([][]byte, MSGCNT)
	SIG    = make([][]byte, MSGCNT)
	PRV    = make([][]byte, MSGCNT)
	PUB    = make([][]byte, MSGCNT)
	CURVE  = btcec.S256()
)

func init() {
	var err error
	for i := 0; i < len(MSG); i++ {
		if prv, err := ethcrypto.GenerateKey(); err != nil {
			panic(err)
		} else {
			blob := prv.D.Bytes()
			PRV[i] = make([]byte, 32)

			copy(PRV[i][32-len(blob):], blob)
			PUB[i] = ethsecp256k1.CompressPubkey(prv.X, prv.Y)
		}

		MSG[i] = make([]byte, 1024)
		if _, err = rand.Read(MSG[i]); err != nil {
			panic(err)
		}

		hash := sha256.Sum256(MSG[i])
		if SIG[i], err = ethsecp256k1.Sign(hash[:], PRV[i]); err != nil {
			panic(err)
		}
	}
	fmt.Println("max core", runtime.GOMAXPROCS(0))
}

func BenchmarkVerify_ethsecp256k1_Single(b *testing.B) {
	for i := 0; i < b.N; i++ {
		idx := i % MSGCNT
		hash := sha256.Sum256(MSG[idx])
		require.True(b, ethsecp256k1.VerifySignature(PUB[idx], hash[:], SIG[idx][:64]))
	}
}

func BenchmarkVerify_ethsecp256k1_Parallel(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		j := 0
		for pb.Next() {
			idx := j % MSGCNT
			hash := sha256.Sum256(MSG[idx])
			require.True(b, ethsecp256k1.VerifySignature(PUB[idx], hash[:], SIG[idx][:64]))
			j++
		}
	})
}

func BenchmarkRecoverVerify_ethsecp256k1_Parallel(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		j := 0
		for pb.Next() {
			idx := j % MSGCNT
			hash := sha256.Sum256(MSG[idx])

			pubKeyBytes, err := ethsecp256k1.RecoverPubkey(hash[:], SIG[idx])
			require.NoError(b, err)

			pubKey, err := btcec.ParsePubKey(pubKeyBytes, btcec.S256())
			require.NoError(b, err)
			//pubKey := &ecdsa.PublicKey{
			//	Curve: CURVE,
			//	X:     new(big.Int).SetBytes(pubKeyBytes[1:33]),
			//	Y:     new(big.Int).SetBytes(pubKeyBytes[33:]),
			//}

			compPubKey := ethsecp256k1.CompressPubkey(pubKey.X, pubKey.Y)

			h0 := sha256.New()
			_, _ = h0.Write(compPubKey)
			sha := h0.Sum(nil)

			h1 := ripemd160.New()
			_, _ = h1.Write(sha)

			addr := h1.Sum(nil)

			require.Equal(b, addr, addr)

			j++
		}
	})
}

func BenchmarkVerify_libsecp256k1_Parallel(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		j := 0
		for pb.Next() {
			idx := j % MSGCNT
			hash := sha256.Sum256(MSG[idx])
			ret := crypto_test.VerifySig(SIG[idx][:64], hash[:], PUB[idx][:])
			require.Equal(b, 1, int(ret))
			j++
		}
	})
}

func BenchmarkVerify_crypto_ecdsa_Single(b *testing.B) {
	for i := 0; i < b.N; i++ {
		idx := i % MSGCNT
		hash := sha256.Sum256(MSG[idx])

		pubKey, err := btcec.ParsePubKey(PUB[idx], btcec.S256())
		require.NoError(b, err)
		ret := ecdsa.Verify((*ecdsa.PublicKey)(pubKey), hash[:], new(big.Int).SetBytes(SIG[idx][:32]), new(big.Int).SetBytes(SIG[idx][32:64]))
		require.True(b, ret)
	}
}

func BenchmarkVerify_crypto_ecdsa_Parallel(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		j := 0
		for pb.Next() {
			idx := j % MSGCNT
			hash := sha256.Sum256(MSG[idx])

			pubKey, err := btcec.ParsePubKey(PUB[idx], btcec.S256())
			require.NoError(b, err)
			ret := ecdsa.Verify((*ecdsa.PublicKey)(pubKey), hash[:], new(big.Int).SetBytes(SIG[idx][:32]), new(big.Int).SetBytes(SIG[idx][32:64]))
			require.True(b, ret)

			j++
		}
	})
}

func TestVerify_libsecp256k1_batch(t *testing.T) {
	start := time.Now()
	hashes := make([][]byte, MSGCNT)
	for i := 0; i < MSGCNT; i++ {
		h := sha256.Sum256(MSG[i])
		hashes[i] = h[:]
	}
	ret := crypto_test.VerifySigBatch(SIG, hashes, PUB)
	require.Equal(t, 1, ret)

	dur := time.Since(start)
	fmt.Println("TestVerify_libsecp256k1_batch: duration", dur.Microseconds(), "us", dur.Microseconds()/int64(MSGCNT), "us/op")
}
