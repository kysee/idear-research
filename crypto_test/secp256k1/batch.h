#ifndef SECP256K1_CL_H
#define SECP256K1_CL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <secp256k1.h>

typedef struct {
	unsigned char data[32];
} secp256k1_msg32;

void hello();

int secp256k1_ecdsa_verify_batch (
    const secp256k1_context* ctx,
    const secp256k1_ecdsa_signature *sig, const size_t sigcnt,
    const secp256k1_msg32* msg32,
    const secp256k1_pubkey *pubkey);

int secp256k1_ecdsa_verify_single (
    const secp256k1_context* ctx,
    const unsigned char* sig,
    const unsigned char* msg32,
    const unsigned char* pubkey,
    const size_t pubkeylen);
#endif