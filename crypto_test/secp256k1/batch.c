#include "batch.h"

void hello() {
	printf("Hello batch.c\n");
}

int print_hex(unsigned char* bz, int len) {
	char out[len*2+1];
//	printf("len: %d, out=%lu\n", len, sizeof(out));
	for (int i=0; i < len; i++) {
		sprintf(&out[i*2], "%02x", bz[i]&0xff);
	}
	out[sizeof(out)-1] = 0;
	printf("%s", out);
	return len*2;
}

int hex2bytes(const char* hex, unsigned char* out, size_t outlen) {
	char *pos = (char*)hex;
	unsigned int byteval;
	size_t count = 0;
    for (; count < outlen; count++) {
        sscanf(pos, "%2x", &byteval);
        out[count] = byteval & 0xff;
        pos += 2;
    }
    return count;
}

int secp256k1_ecdsa_verify_batch (
    const secp256k1_context* ctx,
    const secp256k1_ecdsa_signature *sig, const size_t sigcnt,
    const secp256k1_msg32* msg32,
    const secp256k1_pubkey *pubkey) {

//	printf("[cgo] sig count: %d\n", sigcnt);

	int ret = 0;
    for(size_t i=0; i<sigcnt; i++) {
//    	printf("[cgo] sig:    "); print_hex(sig[i].data, 64); printf("\n");
//    	printf("[cgo] sigcnt:  %d", sigcnt);
//    	printf("[cgo] msg32:  "); print_hex(msg32[i].data, 32); printf("\n");
//    	printf("[cgo] pubkey: "); print_hex(pubkey[i].data, 33); printf("\n");


		ret = secp256k1_ecdsa_verify(ctx, &sig[i], msg32[i].data, &pubkey[i]);
		if (ret != 1) {
			return ret;
		}
	}

	return 1;
}


int secp256k1_ecdsa_verify_single (
    const secp256k1_context* ctx,
    const unsigned char* sig,
    const unsigned char* msg32,
    const unsigned char* pubkey,
    const size_t pubkeylen) {

////	printf("[cgo] sig:    "); print_hex(sig, 64); printf("\n");
//	printf("[cgo] msg32:  "); print_hex(msg32, 32); printf("\n");
////    printf("[cgo] pubkey: "); print_hex(pubkey, 33); printf("\n");

	secp256k1_pubkey _pubkey;
	secp256k1_ec_pubkey_parse(ctx, &_pubkey, pubkey, pubkeylen);

	secp256k1_ecdsa_signature _sig;
	secp256k1_ecdsa_signature_parse_compact(ctx, &_sig, sig);
//	printf("[cgo] sig(serial):"); print_hex(_sig.data, 64); printf("\n");

//	char out[65];
//	size_t outlen = 65;
//	secp256k1_ec_pubkey_serialize(ctx, out, &outlen, &_pubkey, SECP256K1_EC_UNCOMPRESSED);
////	printf("pubkye: "); print_hex(out, 65); printf("\n");

	int r = secp256k1_ecdsa_verify(ctx, &_sig, msg32, &_pubkey);
//	printf("[cgo] result=%d\n", r);

	return r;
}

int test(const secp256k1_context* ctx) {
	secp256k1_ecdsa_signature sig;
	hex2bytes("f218aad78a2f9e443845d2c0effb7a3c2593616a43c57a111ea8973afeb91739f23047fe4ef7b0eadcd7da0d919271b570b1301eed7d58059278c044db48f839", sig.data, 64);

	unsigned char msg32[32];
	hex2bytes("78c806ca719e55fe40b3c89df6cdd8cf5cfaf013e55eb9c06e48fe21b6dbce9b", msg32, 32);

	secp256k1_pubkey pubkey;
	hex2bytes("b8c94b7834e624cce1a77af774ef3878af71c85c88f11aced3ddf14dd1b8ef72f3ac0c9281def094a2949f526ecb1bc8a0c2a25bdae84acc567772f2d78b3e42", pubkey.data, 64);

	print_hex(sig.data, 64); printf("\n");
 	print_hex(msg32, 32); printf("\n");
 	print_hex(pubkey.data, 64); printf("\n");

 	return secp256k1_ecdsa_verify(ctx, &sig, msg32, &pubkey);
}